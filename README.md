## 1. Your first PyScript HTML file
Here's a "Hello, world!" example using PyScript

```html
<html>
  <head>
    <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
    <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
  </head>
  <body>
    <py-script> print('Hello, World!') </py-script>
  </body>
</html>

```

## 2. The py-script tag

```html
<html>
  <head>
    <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
    <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
  </head>
  <body>
      <py-script>
            def multiplication_or_sum(num1, num2):
                # calculate product of two number
                product = num1 * num2
                # check if product is less then 1000
                if product <= 1000:
                    return product
                else:
                    # product is greater than 1000 calculate sum
                    return num1 + num2

            # first condition
            result = multiplication_or_sum(20, 30)
            print("The result is", result)

            # Second condition
            result = multiplication_or_sum(40, 30)
            print("The result is", result)
      </py-script>
  </body>
</html>

```

## 3. Writing into labeled elements

```html
<html>
    <head>
      <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
      <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    </head>

  <body>
    <div id="n1" class="alert alert-primary"></div>
    <div id="n2" class="alert alert-primary"></div>
    <py-script>
def multiplication_or_sum(num1, num2):
    # calculate product of two number
    product = num1 * num2
    # check if product is less then 1000
    if product <= 1000:
        return product
    else:
        # product is greater than 1000 calculate sum
        return num1 + num2

# first condition
result = multiplication_or_sum(20, 30)
pyscript.write('n1', result)

# Second condition
result = multiplication_or_sum(40, 30)
pyscript.write('n2', result)
    </py-script>
  </body>
</html>
```

## 4. Packages and modules
In addition to the Python Standard Library and the pyscript module, many 3rd-party OSS packages will work out-of-the-box with PyScript.

```html
<html>
    <head>
      <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
      <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
      <py-env>
        - numpy
        - matplotlib
      </py-env>
    </head>

  <body>
    <h1>Let's plot random numbers</h1>
    <div id="plot"></div>
    <py-script>
import matplotlib.pyplot as plt
import numpy as np

x = np.random.randn(1000)
y = np.random.randn(1000)

fig, ax = plt.subplots()
ax.scatter(x, y)
pyscript.write('plot', fig)
    </py-script>
  </body>
</html>
```


## 5. Local modules

In addition to packages you can declare local Python modules that will be imported in the <py-script> tag. For example, we can place the random number generation steps in a function in the file data.py.

```html
<html>
    <head>
      <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
      <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
      <py-env>
        - numpy
        - matplotlib
        - paths:
          - /main.py
      </py-env>
    </head>

  <body>
    <h1>Let's plot random numbers</h1>
    <div id="result"></div>
    <py-script output="plot">
from main import multiplication_or_sum

# first condition
result = multiplication_or_sum(20, 30)
pyscript.write('result', result)
    </py-script>
  </body>
</html>

```


# 6. Py REPL

```html

<html>
    <head>
      <link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
      <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
    </head>

  <body>
    <h1>Let's plot random numbers</h1>
    <div>
        <py-repl id="my-repl" auto-generate="true"></py-repl>
    </div>
    
  </body>
</html>

```